module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160069/learn_bootstrap/'
    : '/'
}
